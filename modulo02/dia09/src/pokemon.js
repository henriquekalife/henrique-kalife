class Pokemon {
  constructor( jsonVindoDaApi ) {
    this.nome = jsonVindoDaApi.name
    this.id = jsonVindoDaApi.id
    this.thumbUrl = jsonVindoDaApi.sprites.front_default
    this._altura = jsonVindoDaApi.height * 10
    this.tipos = jsonVindoDaApi.types.map( t => t.type.name )
    this.stats = jsonVindoDaApi.stats.map( t => t.stat.name )
  }
}

const urlApi = 'https://pokeapi.co/api/v2/pokemon'
const pokeApi = new PokeApi( urlApi )

let app = new Vue( {
  el: '#meuPrimeiroApp',
  data: {
    idParaBuscar: '',
    pokemon: { },
    anterior: '0',
    avisoAnterior: false,
    chaveBusca: '1'
  },
  methods: {
    async buscar() {
      if (this.anterior !== this.idParaBuscar) {
        this.pokemon = await pokeApi.buscar( this.idParaBuscar )
        this.avisoAnterior = false
      }
      else {
        this.avisoAnterior = true
      }
      this.anterior = this.idParaBuscar
    },
    async numeroRandom () {

      if (localStorage.getItem(this.idParaBuscar) === this.idParaBuscar) { //detectou numero igual no "estou com sorte"
        this.idAnterior = this.idParaBuscar
        this.idParaBuscar = Math.floor(Math.random() * 802 + 1);
        this.avisoAnterior = false
      }
      else { // não detectou numero igual no "estou com sorte"
        this.idAnterior = this.idParaBuscar
        this.idParaBuscar = Math.floor(Math.random() * 802 + 1);
        this.pokemon = await pokeApi.buscar( this.idParaBuscar )
        this.avisoAnterior = false
      }

      localStorage.setItem(this.idParaBuscar, this.chaveBusca);
      this.chaveBusca++
    }
  }
} )

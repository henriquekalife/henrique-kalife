function rodarPrograma () {
  const h1 = document.getElementById('horario')

  var atualizarH1 = function() {
    h1.innerText = new Date().toLocaleTimeString()
  }
  
  //setInterval ( atualizarH1, 1000 )

  atualizarH1()

  const idIntervalo = setInterval ( atualizarH1, 1000 )

  setInterval ( function () {
    console.log (new Date())
  }, 1500 )

  const btnPararRelogio = document.getElementById('btnPararRelogio')
  btnPararRelogio.onclick = function () {
    clearInterval(idIntervalo)
  }
}

rodarPrograma()

class Jedi {

  constructor (nome) {
    this.nome = nome
    this.h1 = document.createElement('h1')
    this.h1.innerText = this.nome
    this.h1.id = `jedi_${this.nome}`
    const dadosJedi = document.getElementById("dadosJedi")
    dadosJedi.appendChild( this.h1 )
  }

  atacarComSelf () {
    //const h1 = document.getElementById(`jedi_${this.nome}`)
    let self = this //salva contexto da variavel para não dar erro em um suposto 'this' para linha 16.
    setTimeout ( function () {
      self.h1.innerText += ' atacou!'
    }, 1000)
  }

  atacarComBind () {
    setTimeout ( function () {
      self.h1.innerText += ' atacou!'
    }.bind(this), 1000)
  }

  atacarComArrowFunction () {
    setTimeout ( () => {
      this.h1.innerText += ' atacou!'
    }, 1000)
  }
  
}

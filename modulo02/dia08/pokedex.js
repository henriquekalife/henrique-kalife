function rodarPrograma() {
  campoNumeroPokemon = document.getElementById("campoNumeroPokemon")
  campoNumeroPokemon.onblur = () => {

    const $dadosPokemon = document.getElementById('dadosPokemon')
    const $h1 = $dadosPokemon.querySelector( 'h1' )
    const $img = $dadosPokemon.querySelector( 'img' )

    var numeroPokemon = document.getElementById("campoNumeroPokemon").value

    if (numeroPokemon < 1 || numeroPokemon > 802) {
      $h1.innerText="Não encontrado"
    }
    else {
      let url = 'https://pokeapi.co/api/v2/pokemon/' + numeroPokemon + '/' 
  
      fetch( url )
      .then( res => res.json() )
      .then( dadosJson => {
      $h1.innerText=dadosJson.name
      $img.src=dadosJson.sprites.front_default
      } )
    }

  }

}

rodarPrograma()

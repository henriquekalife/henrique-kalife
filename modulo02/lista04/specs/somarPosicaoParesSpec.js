describe( 'somarPosicaoPares', function() {

    beforeEach( function() {
      chai.should()
    } )
  
    it( 'Soma dos números em posição par deve resultar 3.34', function() {
      var array = [1, 56, 4.34, 6, -2]
      const resultado = somarPosicaoPares( array )
      resultado.should.equal( 3.34 )
    } )
  
    it( 'Soma dos números em posição par deve resultar 25', function() {
        var array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        const resultado = somarPosicaoPares( array )
        resultado.should.equal( 25 )
      } )

    it( 'Soma dos números em posição par deve resultar -12', function() {
        var array = [-1, -5, 3, 10, -4, 8, -10]
        const resultado = somarPosicaoPares( array )
        resultado.should.equal( -12 )
    } )

    it( 'Soma dos números em posição par deve resultar -10.91', function() {
        var array = [-1.32, 2, -7.59, 6, 2, 5, -4]
        const resultado = somarPosicaoPares( array )
        resultado.should.equal( -10.91 )
    } )

    it( 'Soma dos números em posição par deve resultar 10', function() {
      var array = [null, null, 4, 5, 6]
      const resultado = somarPosicaoPares( array )
      resultado.should.equal( 10 )
    } )

    it( 'Soma dos números em posição par deve resultar 0', function() {
      var array = [null, null, null, null, null, null]
      const resultado = somarPosicaoPares( array )
      resultado.should.equal( 0 )
    } )

  } )
  
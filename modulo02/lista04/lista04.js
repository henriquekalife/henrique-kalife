console.log("Início do programa!")

//exercício 01

var array = [1, 56, 4.34, 6, -2]

function somarPosicaoPares (array) {
    var resultado = 0
    for (var i = 0; i < array.length; i++) {
        if (i % 2 === 0) {
            resultado += array[i]
        }
    }
    return resultado
}

//exercício 02
function formatarElfos (elfos) {
  elfos.forEach(elfo => {
    elfo.nome = elfo.nome.toUpperCase()
    elfo.temExperiencia = elfo.experiencia > 0
    delete elfo.experiencia

    var textoExperiencia = elfo.temExperiencia? "com": "sem"
    const textoFlechas = elfo.qtdFlechas !== 1? "s" : ""
    
    if (elfo.qtdFlechas === 1){

    }

    elfo.descricao = `${elfo.nome} ${textoExperiencia} experiencia e com${elfo.qtdFlechas} flechas${textoFlechas}`

  });
  return elfos
}


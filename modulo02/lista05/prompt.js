const meuH2 = document.getElementById( 'tituloPagina' )
var status = false

function perguntarNome() { 
  if (!localStorage.cont) {
    localStorage.cont = 5
  }
  
  const nome = prompt( 'Qual seu nome?' )
  meuH2.innerText = nome
  localStorage.nome = nome
  if (localStorage.cont < 0) {
    document.getElementById("btnLimparDados").disabled = true
  }

}

function renderizaNomeArmazenadoNaTela() {
  meuH2.innerText = localStorage.nome
}

const nomeArmazenado = localStorage.nome && localStorage.nome.length > 0
if ( nomeArmazenado ) {
  renderizaNomeArmazenadoNaTela()
} else {
  perguntarNome()
}

function apagarNome() {   
    
    if (localStorage.cont <= 1) {
        document.getElementById("btnLimparDados").disabled = true;
        status = true
    }
    
    document.getElementById("tituloPagina").innerHTML = ""
    localStorage.removeItem('nome')
    localStorage.cont = localStorage.cont - 1
}

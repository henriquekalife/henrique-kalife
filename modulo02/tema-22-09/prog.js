console.log("Início do programa!")

//exercício 01

var raio = 1, tipoCalculo = 'A'

var objeto = {
    raio: 7,
    tipoCalculo: "A"
}

calcularCirculo ( { raio, tipoCalculo } )

function calcularCirculo (objeto) {
  if (objeto.tipoCalculo === 'A') {
    return Math.PI*objeto.raio*objeto.raio
  }
  else if (objeto.tipoCalculo === 'C') {
    return 2*Math.PI*objeto.raio
  }
}

//exercício 02
function naoBissexto (ano) {
    if ((ano % 4 === 0 && ano % 100 != 0) || (ano % 400 === 0)) {
        return false
    }
    else {
        return true
    }
}

//exercício 03
function concatenarSemUndefined (palavra1, palavra2) {
    if (typeof palavra1 === "undefined") {
        palavra1 = ""
    }
    else if (typeof palavra2 === "undefined") {
        palavra2 = ""
    }

    if (palavra1 === "" || palavra2 === "") { //diagrama os espaços no retorno pra ficar bonito (:
        return palavra1 + palavra2
    }
    else {
        return palavra1 + " " + palavra2
    }
}

//exercício 04
function concatenarSemNull (palavra1, palavra2) {
    if (typeof palavra1 === "object") {
        palavra1 = ""
    }
    else if (typeof palavra2 === "object") {
        palavra2 = ""
    }

    if (palavra1 === "" || palavra2 === "") { //igual ao anterior :D
        return palavra1 + palavra2
    }
    else {
        return palavra1 + " " + palavra2
    }
}

//exercício 05
function concatenarEmPaz (palavra1, palavra2) {
    if (typeof palavra1 === "object" || typeof palavra1 === "undefined") {
        palavra1 = ""
    }
    else if (typeof palavra2 === "object" || typeof palavra2 === "undefined") {
        palavra2 = ""
    }

    if (palavra1 === "" || palavra2 === "") { // mesma coisa aqui <3
        return palavra1 + palavra2
    }
    else {
        return palavra1 + " " + palavra2
    }
}

//exercício 06
var adicionar = function(x) {
    return function(y) {
        return x + y
    }
}

//exercício 07
function fiboSum(num){
    var a = 1
    var b = 0
    var temp = 0
    var resultado = 0
  
    while (num >= 0){
        resultado = resultado + b
        temp = a
        a = a + b
        b = temp
        num--
    }
  
    return resultado
}

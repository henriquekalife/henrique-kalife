CREATE TABLE CLIENTE(
   ID NUMBER (38,0) NOT NULL PRIMARY KEY,
   NOME VARCHAR2(50) NOT NULL,
   TELEFONE VARCHAR2(11) NOT NULL,
   ENDERECO VARCHAR2(50) NOT NULL
);

CREATE TABLE ALUGUEL(
   ID NUMBER (38,0) NOT NULL PRIMARY KEY,
   ID_CLIENTE NUMBER (38,0) NOT NULL,
   FOREIGN KEY (ID_CLIENTE) REFERENCES CLIENTE(ID),
   RETIRADA DATE NOT NULL,
   PREVISAO DATE NOT NULL,
   DEVOLUCAO DATE,
   MULTA FLOAT
);

CREATE TABLE FILME(
   ID NUMBER (38,0) NOT NULL PRIMARY KEY,
   TITULO VARCHAR2(30) NOT NULL,
   LANCAMENTO DATE NOT NULL,
   CATEGORIA VARCHAR2(8) CHECK (CATEGORIA IN ('ACAO','AVENTURA','ANIMACAO')) NOT NULL
);

CREATE TABLE MIDIA(
   ID NUMBER (38,0) NOT NULL PRIMARY KEY,
   ID_FILME NUMBER (38,0) NOT NULL,
   ID_ALUGUEL NUMBER (38,0) NOT NULL,
   FOREIGN KEY (ID_FILME) REFERENCES FILME(ID) NOT NULL,
   FOREIGN KEY (ID_ALUGUEL) REFERENCES ALUGUEL(ID),
   TIPO VARCHAR2(10) CHECK (TIPO IN ('VHS', 'DVD', 'BLUE_RAY')),
);

CREATE TABLE VALOR_MIDIA(
   ID NUMBER (38,0) NOT NULL PRIMARY KEY,
   ID_MIDIA NUMBER (38,0) NOT NULL,
   FOREIGN KEY (ID_MIDIA) REFERENCES MIDIA(ID),
   VALOR FLOAT NOT NULL,
   INICIO_VIGENCIA DATE NOT NULL,
   FINAL_VIGENCIA DATE
);

CREATE SEQUENCE VALOR_MIDIA_SEQ;
CREATE SEQUENCE MIDIA_SEQ;
CREATE SEQUENCE ALUGUEL_SEQ;
CREATE SEQUENCE FILME_SEQ;
CREATE SEQUENCE CLIENTE_SEQ;

--DROP TABLE VALOR_MIDIA;
--DROP TABLE MIDIA;
--DROP TABLE FILME;
--DROP TABLE ALUGUEL;
--DROP TABLE CLIENTE;

COMMIT;
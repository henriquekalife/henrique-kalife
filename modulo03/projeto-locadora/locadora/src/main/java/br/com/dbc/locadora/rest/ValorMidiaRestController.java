/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author henrique.kalife
 */
@RestController
@RequestMapping("/api/valormidia")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ValorMidiaRestController extends AbstractRestController<ValorMidia, Long, ValorMidiaService>{
    
    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Override
    public ValorMidiaService getService(){
        return valorMidiaService;
    }
    
}

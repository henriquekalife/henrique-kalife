/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.MidiaType;
import java.io.Serializable;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

/**
 *
 * @author henrique.kalife
 */
@Builder
@Data
@Getter
public class MidiaCatalogoDTO implements Serializable{
    
    private MidiaType tipo;
    private String disp;
    private Double valor;
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.CatalogoDTO;
import java.util.Optional;
import org.springframework.stereotype.Service;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.PrecoDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import br.com.dbc.locadora.repository.FilmeRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author henrique.kalife
 */
@Service
@Getter
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FilmeService extends AbstractCrudService<Filme, Long> {
    
    @Autowired
    private FilmeRepository filmeRepository;
    
    @Autowired
    private MidiaService midiaService;
    
    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Filme> save(FilmeDTO filmeDTO) {
        Filme filme = filmeRepository.save(Filme.builder().titulo(filmeDTO.getTitulo())
                .lancamento(filmeDTO.getLancamento()).categoria(filmeDTO.getCategoria())
                .build());
        
        filmeDTO.getMidia().forEach((midia) -> {
            midiaService.saveMidias(filme, midia);
        });
                
        return filmeRepository.findById(filme.getId());
    }
    
    public Page<Filme> search (Pageable pageable, String titulo, Categoria categoria, LocalDate lancamento) {
        return filmeRepository.findByTituloOrCategoriaOrLancamento(pageable, titulo, categoria, lancamento);
    }
    
    private Filme filmeDTOtoFilme (FilmeDTO filmeDTO) {
        Filme filme = filmeRepository.save(Filme.builder().id(filmeDTO.getId()).titulo(filmeDTO.getTitulo())
                .lancamento(filmeDTO.getLancamento()).categoria(filmeDTO.getCategoria())
                .build());
        
        return filme;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme update (Long id, FilmeDTO filmeDTO) {
        Filme filme = filmeDTOtoFilme(filmeDTO);
        
        filmeDTO.getMidia().forEach(midiaDTO -> midiaService.updateMidia(midiaDTO, filme));
        
        return filme;
    }
    
    private PrecoDTO converteParaValorMidiaDTO(ValorMidia valor) {
        PrecoDTO precoDTO = PrecoDTO.builder().id(valor.getId()).tipo(valor.getIdMidia().getTipo())
                .valorPreco(valor.getValor()).inicioVigencia(valor.getInicioVigencia())
                .finalVigencia(valor.getFinalVigencia()).build();
        return precoDTO;
    }
    
    public Long countByTipo(Long id, MidiaType tipo) {
        return midiaService.countByTipoF(id, tipo);
    }
    
    public List<PrecoDTO> preco (Long idFilme) {
        List<PrecoDTO> precos = new ArrayList<>();
        List<Midia> midias = midiaService.findByIdFilme(idFilme);
        
        midias.forEach((midia) -> {
            List<ValorMidia> valores = valorMidiaService.findByidMidiaId(midia.getId());
            valores.forEach((valor) -> {
                precos.add(converteParaValorMidiaDTO(valor));
            });
        });
        return precos;
    }
    
    public List<CatalogoDTO> procuraCatalogo (Pageable pageable, FilmeDTO filmeDTO) {
        List<CatalogoDTO> catalogo = new ArrayList<>();
        List<Filme> listfilmes = filmeRepository.findByTituloIgnoreCaseContaining(pageable, filmeDTO.getTitulo());
        for (Filme filme : listfilmes) {
            List<Midia> listmidias = midiaService.findByIdFilmeSemAluguel(filme.getId());
            TreeMap<MidiaType, Double> valorDeMidia = new TreeMap<>();
            for (Midia midia : listmidias) {
                if(null != midia.getTipo()) switch (midia.getTipo()) {
                    case DVD:
                        valorDeMidia.put(MidiaType.DVD, valorMidiaService.findByMidia(midia));
                        break;
                    case VHS:
                        valorDeMidia.put(MidiaType.VHS, valorMidiaService.findByMidia(midia));
                        break;
                    case BLUE_RAY:
                        valorDeMidia.put(MidiaType.BLUE_RAY, valorMidiaService.findByMidia(midia));
                        break;
                    default:
                        break;
                }
            }
            catalogo.add(criaCatalogo(pageable, filme, listmidias.size() > 0, valorDeMidia));
        }
        return catalogo;
    }
    
    private LocalDate verificaMenorDataDisponivel (Pageable pageable) {
        LocalDate menorData = null;
        if (aluguelService.devolucaoHoje(pageable) != null) {
            menorData = LocalDate.now();
        }
        else {
            int i = 0;
            while (menorData == null) {
                if (aluguelService.devolucaoHoje(pageable) != null) {
                    break;
                }
                else {
                    menorData = LocalDate.now().plusDays(i);
                    i++;
                }
            }
        }
        
        return menorData;
    }
    
    
    private CatalogoDTO criaCatalogo(Pageable pageable, Filme filme, boolean verificaDisponivel, TreeMap<MidiaType, Double> valorDeMidia) {
        CatalogoDTO catalogoDTO = CatalogoDTO.builder().filme(filme)
                .disp(verificaDisponivel ? "Disponivel" : "Indisponível")
                .diaDisponivel(verificaDisponivel ? verificaMenorDataDisponivel(pageable) : LocalDate.now() )
                .valorDeMidia(valorDeMidia).build();
        return catalogoDTO;
    }
    
}

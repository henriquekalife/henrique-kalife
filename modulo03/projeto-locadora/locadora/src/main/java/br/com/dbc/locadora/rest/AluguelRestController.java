/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import br.com.dbc.locadora.service.MidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author henrique.kalife
 */
@RestController
@RequestMapping("/api/aluguel")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class AluguelRestController extends AbstractRestController<Aluguel, Long, AluguelService> {
    
    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private MidiaService midiaService;
    
    @Override
    public AluguelService getService(){
        return aluguelService;
    }
    
    @PostMapping("/retirada")
    public ResponseEntity<?> retirar(@RequestBody AluguelDTO input) {
        if (input.getIdCliente() == null || input.getMidias() == null || input.getMidias().isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(aluguelService.save(input));
    }
    
    @PostMapping("/devolucao")
    public void devolucao (@RequestBody DevolucaoDTO input) {
        if (input.getMidias().isEmpty() == false) {
            midiaService.devolver(input);
        }
    }
    
    @GetMapping("/devolucao")
    public ResponseEntity<?> devolucaoHoje(Pageable pageable) {
        return ResponseEntity.ok(aluguelService.devolucaoHoje(pageable));
    }
        
}

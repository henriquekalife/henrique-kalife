/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Midia;
import java.io.Serializable;
import java.util.ArrayList;
import lombok.Data;

/**
 *
 * @author henrique.kalife
 */
@Data
public class AluguelDTO implements Serializable {
    
    private Long idCliente;
    private ArrayList<Long> midias;
    
}

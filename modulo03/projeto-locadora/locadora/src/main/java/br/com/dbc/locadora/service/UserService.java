/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.domain.User;
import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author henrique.kalife
 */
@Service
public class UserService extends AbstractCrudService<User, Long> {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    @Override
    protected JpaRepository<User, Long> getRepository() {
        return userRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User atualizarSenha(UserDTO dto) {
        User u = userRepository.findByUsername(dto.getUsername());
        
        u.setPassword(dto.getPassword());
        
        super.save(u);
        
        return u;
    }
    
}

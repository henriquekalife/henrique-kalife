/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author henrique.kalife
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {

    public Long countByTipo(MidiaType tipo);
    
    public List<Midia> findByFilme (Filme idFilme);
    
    public List<Midia> findByFilmeId (Long idFilme);
    
    public List<Midia> findByFilmeIdAndAluguelIsNull (Long idFilme);
    
    public Page<Midia> findByFilmeTituloContainingIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween(Pageable pageable, String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import lombok.Data;
import java.time.LocalDate;
import br.com.dbc.locadora.entity.Categoria;
import java.io.Serializable;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;

/**
 *
 * @author henrique.kalife
 */
@Data
@Builder
public class FilmeDTO implements Serializable{
    
    private Long id;
    private String titulo;
    
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDate lancamento;
    
    private Categoria categoria;
    private ArrayList<MidiaDTO> midia;
    
}

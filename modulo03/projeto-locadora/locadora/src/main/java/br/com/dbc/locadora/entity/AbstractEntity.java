/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

/**
 *
 * @author henrique.kalife
 * @param <ID>
 */
public abstract class AbstractEntity<ID> {
    
    public abstract ID getId();
    
}

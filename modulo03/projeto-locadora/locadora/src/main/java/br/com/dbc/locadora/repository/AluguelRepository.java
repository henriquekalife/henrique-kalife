/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author henrique.kalife
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {

    public Page<Aluguel> findByPrevisaoEqualsAndDevolucaoIsNull(Pageable pageable, LocalDate localdate);
    public Page<Aluguel> findByPrevisao(Pageable pageable, LocalDate localdate);
    
}

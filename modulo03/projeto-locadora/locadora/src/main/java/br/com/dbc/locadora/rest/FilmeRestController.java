/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author henrique.kalife
 */
@RestController
@RequestMapping("/api/filme")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class FilmeRestController extends AbstractRestController<Filme, Long, FilmeService> {
    
    @Autowired
    private FilmeService filmeService;
    
    @Override
    public FilmeService getService(){
        return filmeService;
    }
    
    @PostMapping("/midia")
    public ResponseEntity<?> post(@RequestBody FilmeDTO input) {
        if (input.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(filmeService.save(input));
    }
    
    @GetMapping("/search")
    public ResponseEntity<Page<Filme>> get(Pageable pageable, 
                @RequestParam(value = "categoria", required = false)Categoria categoria, 
                @RequestParam(value = "titulo", required = false)String titulo, 
                @RequestParam(value = "lancamento", required = false)@DateTimeFormat(pattern = "dd/MM/yyyy")LocalDate lancamento) {
        
        return ResponseEntity.ok(filmeService.search(pageable, titulo, categoria, lancamento));
    }
    
    @PutMapping("/{id}/midia")
    public ResponseEntity<?> update (@PathVariable Long id, @RequestBody FilmeDTO filmeDTO) {
        
        if(!id.equals(filmeDTO.getId())) return ResponseEntity.badRequest().build();
        
        return ResponseEntity.ok(filmeService.update(id, filmeDTO));
        
    }
    
    @GetMapping("/count/{id}/{tipo}")
    public ResponseEntity<?> count (@PathVariable Long id, @PathVariable MidiaType tipo) {
        return ResponseEntity.ok(filmeService.countByTipo(id, tipo));
    }
    
    @GetMapping("/precos/{id}")
    public ResponseEntity<?> precos (@PathVariable Long id) {
        if(id == null) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(filmeService.preco(id));
        }
    }
    
    @PostMapping("/search/catalogo")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public ResponseEntity<?> catalogo (Pageable pageable, @RequestBody FilmeDTO filmeDTO) {
        return ResponseEntity.ok(filmeService.procuraCatalogo(pageable, filmeDTO));
    }
    
}

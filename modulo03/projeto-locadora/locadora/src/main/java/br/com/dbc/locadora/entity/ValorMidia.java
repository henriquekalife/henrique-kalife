/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author henrique.kalife
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ValorMidia extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(name = "VALOR")
    private double valor;
    
    @NotNull
    @Column(name = "INICIO_VIGENCIA")
    @DateTimeFormat(pattern = "DD/MM/YYYY")
    private LocalDateTime inicioVigencia;
    
    @Column(name = "FINAL_VIGENCIA")
    @DateTimeFormat(pattern = "DD/MM/YYYY")
    private LocalDateTime finalVigencia;
    
    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Midia idMidia;

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author henrique.kalife
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {

    public Optional<ValorMidia> findByidMidia (Midia midia);
    
    public void deleteByIdMidiaId (Long idMidia);
    
    public Optional<ValorMidia> findByidMidiaAndFinalVigenciaIsNull(Midia idMidia);

    public List<ValorMidia> findByidMidiaId (Long idFilme);
}

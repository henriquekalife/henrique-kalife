/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.domain.User;
import br.com.dbc.locadora.dto.UserDTO;
import br.com.dbc.locadora.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author henrique.kalife
 */
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class UserRestController extends AbstractRestController<User, Long, UserService> {
    
    @Autowired
    private UserService userService;
    
    @Override
    public UserService getService(){
        return userService;
    }
    
    @PutMapping("/password")
    public ResponseEntity<?> changePassword(@RequestBody UserDTO userDTO) {
        if(userDTO.getUsername()== null || userDTO.getPassword() == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(userService.atualizarSenha(userDTO));
    }
    
}

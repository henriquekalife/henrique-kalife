/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author henrique.kalife
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cliente extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "NOME")
    private String nome;
    
    @NotNull
    @Size(min = 7, max = 11)
    @Column(name = "TELEFONE")
    private String telefone;
    
    @Size(min = 1, max = 50)
    @Column(name = "RUA")
    private String rua;
    
    @Size(min = 1, max = 50)
    @Column(name = "BAIRRO")
    private String bairro;
    
    @Size(min = 1, max = 50)
    @Column(name = "CIDADE")
    private String cidade;
    
    @Size(min = 1, max = 50)
    @Column(name = "ESTADO")
    private String estado;

}

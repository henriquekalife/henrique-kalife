/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author henrique.kalife
 */
@Builder
@Data
public class CatalogoDTO {
    
    private Filme filme;
    private List<Midia> midia;
    
    private String disp;
    private LocalDate diaDisponivel;
    private TreeMap<MidiaType, Double> valorDeMidia;
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.repository.AluguelRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author henrique.kalife
 */
@Service
public class AluguelService extends AbstractCrudService<Aluguel, Long>{
    
    @Autowired
    private AluguelRepository aluguelRepository;
    
    @Autowired
    private MidiaService midiaService;
    
    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Aluguel> save (AluguelDTO aluguelDTO) {
        
        Aluguel aluguel = aluguelRepository.save(Aluguel.builder().retirada(LocalDate.now())
                .previsao(LocalDate.now()).devolucao(null).multa(0.0)
                .idCliente(clienteService.findById(aluguelDTO.getIdCliente())
                .orElseGet(null)).build());
        
        aluguelDTO.getMidias().forEach((midia) -> {
            Midia md = midiaService.findById(midia).orElseGet(null);
            if (md.getAluguel() == null) {
                midiaService.edit(md, aluguel);
            }
        });
        
        return findById(aluguel.getId());
    }
    
    public Page<Aluguel> devolucaoHoje(Pageable pageable) {                
        return aluguelRepository.findByPrevisaoEqualsAndDevolucaoIsNull(pageable, LocalDate.now());
    }
    
}

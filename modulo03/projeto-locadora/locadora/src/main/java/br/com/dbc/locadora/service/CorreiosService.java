/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.dto.CepDTO;
import br.com.dbc.locadora.ws.ConsultaCEP;
import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import br.com.dbc.locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author henrique.kalife
 */
@Service
public class CorreiosService {
    
    private final String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";
    
    @Autowired
    private SoapConnector soapConnector;

    @Autowired
    private ObjectFactory objectFactory;
    
    
    public CepDTO buscarCEP(String cep) {
        ConsultaCEP consultaCEP = objectFactory.createConsultaCEP();
        consultaCEP.setCep(cep);
        
        ConsultaCEPResponse respostaCEP = ((JAXBElement<ConsultaCEPResponse>) soapConnector
                .callWebService(url, objectFactory.createConsultaCEP(consultaCEP))).getValue();
        
        return CepDTO.builder().rua(respostaCEP.getReturn().getEnd())
                .bairro(respostaCEP.getReturn().getBairro())
                .cidade(respostaCEP.getReturn().getCidade())
                .estado(respostaCEP.getReturn().getUf())
                .build();
    }
    
    
}

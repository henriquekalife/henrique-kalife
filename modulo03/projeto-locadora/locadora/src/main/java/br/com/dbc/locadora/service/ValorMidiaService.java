/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.ValorMidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import java.time.LocalDateTime;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



/**
 *
 * @author henrique.kalife
 */
@Service
@Getter
public class ValorMidiaService extends AbstractCrudService<ValorMidia, Long> {
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Autowired
    private MidiaService midiaService;
    
    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void savePreco(Midia midia, Double valor){
        
        valorMidiaRepository.save(ValorMidia.builder().valor(valor)
                .inicioVigencia(LocalDateTime.now()).finalVigencia(LocalDateTime.now())
                .idMidia(midia).build());
        
    }

    public double findByMidia (Midia midia) {
        return valorMidiaRepository.findByidMidia(midia).orElseGet(null).getValor(); 
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteByIdMidiaId(Midia idMidia) {
        valorMidiaRepository.deleteByIdMidiaId(idMidia.getId());
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateByIdMidia(Midia idMidia, Double valor) {
        Optional<ValorMidia> optionalMidia = valorMidiaRepository
                        .findByidMidiaAndFinalVigenciaIsNull(idMidia);
        if (!optionalMidia.isPresent()) {
            return;
        }        
        ValorMidia valorMidia = optionalMidia.get();        
        if (valorMidia.getValor() == valor) {
            return;
        }
        valorMidia.setFinalVigencia(LocalDateTime.now());
        
        valorMidiaRepository.save(valorMidia);
        
        valorMidiaRepository.save(ValorMidia.builder().id(null).valor(valor)
                .inicioVigencia(LocalDateTime.now()).finalVigencia(null)
                .idMidia(idMidia).build()
        );
    }
    
    public List<ValorMidia> findByidMidiaId(Long idFilme) {
        return valorMidiaRepository.findByidMidiaId(idFilme);
    }
    
}

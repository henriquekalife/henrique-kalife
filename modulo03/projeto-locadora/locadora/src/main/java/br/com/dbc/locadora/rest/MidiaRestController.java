 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author henrique.kalife
 */
@RestController
@RequestMapping("/api/midia")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class MidiaRestController extends AbstractRestController<Midia, Long, MidiaService>{
    
    @Autowired
    private MidiaService midiaService;
    
    @Override
    public MidiaService getService(){
        return midiaService;
    }
    
    @GetMapping("/count/{tipo}")
    public ResponseEntity<?> countMidia(@PathVariable MidiaType tipo) {
        return ResponseEntity.ok(midiaService.countByTipo(tipo));
    }
    
    @GetMapping("/search")
    public ResponseEntity<Page<Midia>> searchMidia(Pageable pageable,
            @RequestParam(value = "categoria", required = false) Categoria categoria, 
            @RequestParam(value = "titulo", defaultValue = "", required = false) String titulo, 
            @RequestParam(value = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(value = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoFim){        
        return ResponseEntity.ok(midiaService.procuraMidia(pageable, titulo, categoria, lancamentoIni, lancamentoFim));
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import java.io.Serializable;
import br.com.dbc.locadora.entity.MidiaType;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author henrique.kalife
 */
@Data
@Builder
public class MidiaDTO implements Serializable {
    
    private MidiaType tipo;
    private Integer quantidade;
    private Double valor;
    
}

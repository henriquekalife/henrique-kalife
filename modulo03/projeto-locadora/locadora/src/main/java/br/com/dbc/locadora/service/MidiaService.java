/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.DevolucaoDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author henrique.kalife
 */
@Service
public class MidiaService extends AbstractCrudService<Midia, Long> {
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveMidias(Filme filme, MidiaDTO midiaDTO){
        for(int i = 0; i < midiaDTO.getQuantidade(); i++){
            Midia midia = Midia.builder().tipo(midiaDTO.getTipo())
                .aluguel(null).filme(filme).build();
        
            midiaRepository.save(midia);
            valorMidiaService.savePreco(midia, midiaDTO.getValor());
        }
    }
    
    public void edit(Midia midia, Aluguel aluguel) {
        midia.setAluguel(aluguel);
        midiaRepository.save(midia);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void devolver(DevolucaoDTO midias) {
        midias.getMidias().forEach((midia) -> {
            Midia m = midiaRepository.findById(midia).orElseGet(null);
            Aluguel a = m.getAluguel();
            a.setDevolucao(LocalDateTime.now());
            if (a.getDevolucao().getDayOfMonth() != a.getPrevisao().getDayOfMonth() && a.getDevolucao().getHour() >= 16) {
                a.setMulta(valorMidiaService.findByMidia(m));
            }
            this.edit(m, null);
            aluguelService.save(a);
        });
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<Midia> deletarMidias(int tam, List<Midia> midias) {
        List<Midia> midiasLivres = midias.stream().filter(m -> m.getAluguel() == null).collect(Collectors.toList());
        for (int i = 0; i < tam; i++) {
            valorMidiaService.deleteByIdMidiaId(midiasLivres.get(0));
            delete(midiasLivres.get(0).getId());
            midiasLivres.remove(0);
        }
        return midiasLivres;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateMidia(MidiaDTO dto, Filme filme) {
        List<Midia> midiasAtuais = midiaRepository.findByFilme(filme).stream()
                .filter((midia) -> {
                    return midia.getTipo().equals(dto.getTipo());
                }).collect(Collectors.toList());
        
        int diferenca = dto.getQuantidade() - midiasAtuais.size();
        
        if (diferenca < 0) {
            deletarMidias(-1 * diferenca, midiasAtuais);
        }
        
        midiasAtuais.forEach((m) -> {
            valorMidiaService.updateByIdMidia(m, dto.getValor());
        });
        if (diferenca > 0) {
            dto.setQuantidade(diferenca);
            saveMidias(filme, dto);
        }
    }
    
    public Long countByTipoF(Long id, MidiaType tipo) {
        return midiaRepository.countByTipo(tipo);
    }
    
    public Long countByTipo(MidiaType tipo) {
        return midiaRepository.countByTipo(tipo);
    }
    
    public List<Midia> findByIdFilme(Long idFilme) {
        return midiaRepository.findByFilmeId(idFilme);
    }
    
    public List<Midia> findByIdFilmeSemAluguel(Long idFilme) {
        return midiaRepository.findByFilmeIdAndAluguelIsNull(idFilme);
    }
    
    public Page<Midia> procuraMidia(
            Pageable pageable, String titulo, Categoria categoria, 
            LocalDate lancamentoIni, LocalDate lancamentoFim){
        
        if (lancamentoIni == null) lancamentoIni = LocalDate.MIN;
        if (lancamentoFim == null) lancamentoFim = LocalDate.MIN;
        
        return midiaRepository.findByFilmeTituloContainingIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween
                (pageable, titulo, categoria, lancamentoIni, lancamentoFim);
    }
    
}

package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.AbstractEntity;
import br.com.dbc.locadora.service.AbstractCrudService;
import java.util.Objects;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
*
* @author henrique.kalife
* @param <E>
* @param <SERVICE>AbstractEntity
*/
@PreAuthorize("hasAuthority('ADMIN_USER')")
public abstract class AbstractRestController<E extends AbstractEntity<ID>, ID, SERVICE extends AbstractCrudService<E, ID>> {

  protected abstract SERVICE getService();

  @GetMapping()
  public ResponseEntity<?> list(Pageable pageable) {
      return ResponseEntity.ok(getService().findAll(pageable));
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> get(@PathVariable Long id) {
      return getService().findById(id)
              .map(ResponseEntity::ok)
              .orElse(ResponseEntity.notFound().build());
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> put(@PathVariable Long id, @RequestBody E input) {
      if (id == null || !Objects.equals(input.getId(), id)) {
          return ResponseEntity.badRequest().build();
      }
      return ResponseEntity.ok(getService().save(input));
  }

  @PostMapping
  public ResponseEntity<?> post(@RequestBody E input) {
      if (input.getId() != null) {
          return ResponseEntity.badRequest().build();
      }
      return ResponseEntity.ok(getService().save(input));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable Long id) {
      getService().delete(id);
      return ResponseEntity.noContent().build();
  }

}
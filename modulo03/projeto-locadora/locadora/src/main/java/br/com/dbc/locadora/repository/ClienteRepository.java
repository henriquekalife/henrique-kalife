/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Cliente;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author henrique.kalife
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    //public List<Cliente> findByCep (Pageable pageable, String cep);
    //public List<Cliente> findByNomeContainingIgnoreCase (Pageable pageable, String nome);
    //public List<Cliente> findByBairroIgnoreCase (Pageable pageable, String endereco);
    
    
}

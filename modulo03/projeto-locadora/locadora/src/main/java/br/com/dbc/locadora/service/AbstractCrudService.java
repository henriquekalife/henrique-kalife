/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author henrique.kalife
 * @param <E>
 */
@AllArgsConstructor
@Transactional(readOnly = true)
public abstract class AbstractCrudService<E, ID> {
    
    protected abstract JpaRepository<E, Long> getRepository();      
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save (@NotNull @Valid E e) {
        return getRepository().save(e);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete (Long id) {
        getRepository().deleteById(id);
    }
    
    public Optional<E> findById (Long id) {
        return getRepository().findById(id);
    }
    
    public Page<E> findAll (Pageable pageable) {
        return getRepository().findAll(pageable);
    }
    
}

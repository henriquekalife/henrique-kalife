/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static java.time.format.DateTimeFormatter.ofPattern;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author henrique.kalife
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private FilmeService filmeService;
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Override
    protected AbstractRestController getController() {
        return filmeRestController;
    }
    
    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void filmeCreateTest() throws Exception {
        
        Filme f = Filme.builder().titulo("007 Goldeneye").lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO).build();
                
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("25/04/1993"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(f.getCategoria(), filmes.get(0).getCategoria());
        
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void filmeMidiaTest() throws Exception {
                
        ArrayList<MidiaDTO> midias = new ArrayList<>();  
        
        MidiaDTO midiaDTO = MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.2).build();
        midias.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.2).build());
        
        FilmeDTO filmeDTO = FilmeDTO.builder().titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25)).categoria(Categoria.ACAO)
                .midia(midias).build();
                
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("25/04/1993"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().toString()));
        
        List<Filme> filmes = filmeRepository.findAll();
        List<Midia> midiasBanco = midiaRepository.findAll();
        //List<ValorMidia> valorMidias = valorMidiaRepository.findAll();
        List<Midia> listmidias = midiaRepository.findAll();
        
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(5, midiasBanco.size());
        Assert.assertEquals(1, midias.size());
        Assert.assertEquals(listmidias.get(0).getTipo(), filmeDTO.getMidia().get(0).getTipo());
        
        Assert.assertEquals(1, midias.size());
        Assert.assertEquals(midiaDTO.getTipo(), listmidias.get(0).getTipo());
        Assert.assertEquals(filmes.get(0), listmidias.get(0).getFilme());
     
        List<ValorMidia> valores = valorMidiaRepository.findAll();
        Assert.assertEquals(5, valores.size());
        Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm")), valores.get(0).getInicioVigencia().format(ofPattern("dd/MM/yyyy HH:mm")));
        Assert.assertNotNull(valores.get(0).getFinalVigencia());
        
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void updateFilmeTest() throws Exception {
        List<MidiaDTO> midiasDTO = Arrays.asList(
                MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(1.9d).build(),
                MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(1.9d).build(),
                MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(1.9d).build()
        );

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("João e o Feijão")
                .lancamento(LocalDate.of(2018, 10, 10))
                .categoria(Categoria.AVENTURA)
                .midia(new ArrayList<>(midiasDTO))
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)));

        Filme filmeSave = filmeRepository.findAll().get(0);

        filmeDTO.setId(filmeSave.getId());
        filmeDTO.getMidia().forEach(midiaDTO -> {
            midiaDTO.setValor(9.0d);
        });

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/" + filmeDTO.getId() + "/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("10/10/2018"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().name()));

        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(filmeDTO.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filmeDTO.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(filmeDTO.getCategoria(), filmes.get(0).getCategoria());

        List<Midia> midias = midiaRepository.findAll();
        Assert.assertEquals(3, midias.size());
        Assert.assertEquals(midiasDTO.get(0).getTipo(), midias.get(0).getTipo());
        Assert.assertEquals(filmes.get(0), midias.get(0).getFilme());

        List<ValorMidia> valorMidias = valorMidiaRepository.findAll();

        Assert.assertEquals(3, valorMidias.size());

        List<ValorMidia> valorMidiasAntigos = valorMidias.stream().filter(v -> v.getFinalVigencia() != null).collect(Collectors.toList());
        Assert.assertEquals(3, valorMidiasAntigos.size());

        valorMidiasAntigos.stream().forEach(v -> {
            Assert.assertEquals(1.9d, v.getValor(), 0.01d);
            Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm")),
                     v.getInicioVigencia().format(ofPattern("dd/MM/yyyy HH:mm")));
            Assert.assertEquals(LocalDateTime.now().format(ofPattern("dd/MM/yyyy HH:mm")),
                    v.getFinalVigencia().format(ofPattern("dd/MM/yyyy HH:mm")));
        });

    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void buscaCatalogoTest() throws Exception {
        Filme f = Filme.builder()
                .titulo("007 Goldeneye")
                .lancamento(LocalDate.of(1993, 04, 25))
                .categoria(Categoria.ACAO)
                .build();

        Midia m = Midia.builder()
                .tipo(MidiaType.VHS)
                .filme(f)
                .build();

        ArrayList<MidiaDTO> midias = new ArrayList<>();

        MidiaDTO midiaDTO = MidiaDTO.builder()
                .tipo(m.getTipo())
                .quantidade(5)
                .valor(3.2)
                .build();

        midias.add(midiaDTO);

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo(f.getTitulo())
                .lancamento(f.getLancamento())
                .categoria(f.getCategoria())
                .midia(midias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value("25/04/1993"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value("ACAO"));

        FilmeDTO filmeDTOBusca = FilmeDTO.builder()
                .titulo("007")
                .build();

    }
    
}

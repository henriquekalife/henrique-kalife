/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.domain.User;
import br.com.dbc.locadora.repository.UserRepository;
import br.com.dbc.locadora.service.UserService;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author henrique.kalife
 */
public class UserRestControllerTest extends LocadoraApplicationTests {
    
    @Autowired
    private UserRestController userRestController;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserService userService;

    @Override
    protected AbstractRestController getController() {
        return userRestController;
    }
    
    @Before
    public void beforeTest() {
        userRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void userCreateTest() throws Exception {
        
        User u = User.builder().username("henriquekalife").password("123hk")
                .firstName("Henrique").lastName("Kalife").build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(u)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(u.getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(u.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(u.getLastName()));
        
        List<User> users = userRepository.findAll();
                
        Assert.assertEquals(1, users.size());
        Assert.assertEquals(u.getFirstName(), users.get(0).getFirstName());
        Assert.assertEquals(u.getLastName(), users.get(0).getLastName());
        Assert.assertEquals(u.getUsername(), users.get(0).getUsername());
        
    }
    
}

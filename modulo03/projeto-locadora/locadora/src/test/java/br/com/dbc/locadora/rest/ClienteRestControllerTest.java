/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.domain.User;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author henrique.kalife
 */

public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected AbstractRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    /*
    @Test
    @WithMockUser(username="john.doe", 
            password = "jwtpass", 
            authorities = {"STANDARD_USER"})
    */
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void clienteCreateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").telefone("999999999").rua("ruass").build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getRua(), clientes.get(0).getRua());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void clienteBuscarIdTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").telefone("999999999").rua("rua").build();
        
        Cliente c1ienteSalvo = objectMapper.readValue(
            restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(objectMapper.writeValueAsBytes(c)))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andReturn().getResponse().getContentAsString(), Cliente.class);
        
         Cliente c1 = objectMapper.readValue(restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/cliente/%d", c1ienteSalvo.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), Cliente.class);
         
         Assert.assertEquals(c.getNome(), c1.getNome());
         Assert.assertEquals(c.getTelefone(), c1.getTelefone());
         Assert.assertEquals(c.getRua(), c1.getRua());
    }
    
    //@Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void clienteBuscarTelefoneTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome").rua("rua").telefone("99999999").build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()));
        
        Cliente c1 = objectMapper.readValue(restMockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/cliente/buscar/telefone/%s", c.getTelefone()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), Cliente.class);
         
         Assert.assertEquals(c.getNome(), c1.getNome());
         Assert.assertEquals(c.getTelefone(), c1.getTelefone());
         Assert.assertEquals(c.getRua(), c1.getRua());
    }

}

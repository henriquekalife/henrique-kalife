/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

/**
 *
 * @author henrique.kalife
 */
@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS {

    @EJB
    private ProdutoDAO produtoDAO;// Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"

    public ProdutoDAO getDAO() {
        return produtoDAO;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.dao.PersistenceUtils;
import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.mockito.internal.util.reflection.Whitebox;

/**
 *
 * @author henrique.kalife
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        em.getTransaction().begin();
        em.createQuery("delete from Cliente");
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDAO method, of class ClienteService.
     */
    @Test
    public void testGetDAO() {
        System.out.println("getDAO");

    }

    private final EntityManager em = PersistenceUtils.getEntityManager();
    /**
     * Test of findAll method, of class ClienteService.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
                .nome("João")
                .animalList(Arrays.asList(Animal
                        .builder()
                        .nome("Pé de Feijão")
                        .build()))
                    .build();
        em.persist(c);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> clientes = instance.findAll();
        
        assertEquals(1, clientes.size());
        assertEquals(c.getId(),
                clientes.stream().findFirst().get().getId());
        
        Cliente nullC = null;
        
        Optional.ofNullable(nullC).orElseGet(() -> new Cliente());
        
        assertEquals(c.getAnimalList().size(),
                clientes.stream().findFirst().get().getAnimalList().size());
        
        assertEquals(c.getAnimalList().get(0).getId(),
                clientes.stream().findFirst().get().getAnimalList().get(0).getId());
        
        assertEquals(c.getAnimalList().get(0).getId(),
               clientes.stream().findFirst().get().getAnimalList().get(0).getId());
        
        assertEquals(true, !false); //:D
        
    }

    /**
     * Test of findOne method, of class ClienteService.
     */
    @Test
    public void testFindOne() {
        System.out.println("findOne");

    }

    /**
     * Test of create method, of class ClienteService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
                .nome("Irineu")
                .animalList(Arrays.asList(Animal
                        .builder()
                        .nome("Vc nao sabe nem eu")
                        .build()))
                    .build();
        em.persist(c);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> clientes = instance.findAll();
        
        assertEquals(2, clientes.size());
        assertEquals("Irineu", c.getNome());
        
    }

    /**
     * Test of update method, of class ClienteService.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
                .nome("Andrés")
                .animalList(Arrays.asList(Animal
                        .builder()
                        .nome("")
                        .build()))
                    .build();
        em.persist(c);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> clientes = instance.findAll();
        
        c.setNome("D'Alessandro"); //atualiza nome
        
        assertEquals(3, clientes.size()); //verifica se foi criado
        assertEquals("D'Alessandro", c.getNome());

    }

    /**
     * Test of delete method, of class ClienteService.
     */
    @Test
    public void testDelete() {
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente created = Cliente.builder()
                .id(1l)
                .nome("Jovem")
                .animalList(Arrays
                        .asList(Animal.builder()
                            .id(1l)
                            .nome("Velho")
                            .build()))
                .build();
        Mockito.doReturn(created).when(clienteDAO).findOne(created.getId());
        Cliente returned = clienteService.findOne(created.getId());
        assertEquals(created.getId(), returned.getId());
        Mockito.verify(clienteDAO, times(1)).findOne(created.getId());
    }
    
    @Test
    public void testFindOneMocked(){
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente created = Cliente.builder()
                .id(1l)
                .nome("Jovem")
                .animalList(Arrays
                        .asList(Animal.builder()
                            .id(1l)
                            .nome("Velho")
                            .build()))
                .build();
        Mockito.doReturn(created).when(clienteDAO).findOne(created.getId());
        Cliente returned = clienteService.findOne(created.getId());
        assertEquals(created.getId(), returned.getId());
        Mockito.verify(clienteDAO, times(1)).findOne(created.getId());
    }
    
}

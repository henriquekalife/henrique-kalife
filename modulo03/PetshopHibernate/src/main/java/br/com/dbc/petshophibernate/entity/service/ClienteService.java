/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.entity.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.entity.Cliente;

/**
 *
 * @author henrique.kalife
 */
public class ClienteService extends AbstractCRUDService< Cliente, Long, ClienteDAO > {
    
    private static ClienteService instance;
    
    static{
        instance = new ClienteService();
    }
    
    public static ClienteService getInstance(){
        return instance;
    }
    
    private ClienteService(){}
    
    @Override
    protected ClienteDAO getDao() {
        return ClienteDAO.getInstance();
    }
   
}

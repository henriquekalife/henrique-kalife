import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    private final double DELTA = 0.1;
    
    @Test
    public void testaMedia () {
        Inventario inventario = new Inventario();
        EstatisticasInventario est = new EstatisticasInventario(inventario);
        Item lanca = new Item("Lança", 3);
        Item espada = new Item("Espada de aço valiriano", 3);
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        assertEquals(3, est.calcularMedia(), DELTA);
    }
    
    @Test
    public void testaMediana () {
        Inventario inventario = new Inventario();
        EstatisticasInventario est = new EstatisticasInventario(inventario);
        
        Item lanca1 = new Item("Lança 1", 1);
        Item lanca2 = new Item("Lança 2", 3);
        Item lanca3 = new Item("Lança 3", 3);
        Item lanca4 = new Item("Lança 4", 3);
        Item lanca5 = new Item("Lança 5", 5);
        Item lanca6 = new Item("Lança 6", 5);
        Item lanca7 = new Item("Lança 7", 5);
        Item lanca8 = new Item("Lança 8", 8);
        
        inventario.adicionar(lanca1);
        inventario.adicionar(lanca2);
        inventario.adicionar(lanca3);
        inventario.adicionar(lanca4);
        inventario.adicionar(lanca5);
        inventario.adicionar(lanca6);
        inventario.adicionar(lanca7);
        inventario.adicionar(lanca8);
        
        assertEquals(4, est.calcularMediana(), DELTA);
    }
    
    @Test
    public void testaQtdItensAcimaDaMedia () {
        Inventario inventario = new Inventario();
        EstatisticasInventario est = new EstatisticasInventario(inventario);
        
        Item lanca1 = new Item("Lança 1", 10);
        Item lanca2 = new Item("Lança 2", 1);
        Item lanca3 = new Item("Lança 3", 2);
        Item lanca4 = new Item("Lança 4", 3);
        
        inventario.adicionar(lanca1);
        inventario.adicionar(lanca2);
        inventario.adicionar(lanca3);
        inventario.adicionar(lanca4);
        
        assertEquals(1, est.qtdItensAcimaDaMedia(), DELTA);
    }
    
}
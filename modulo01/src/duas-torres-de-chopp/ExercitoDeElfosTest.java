import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoDeElfosTest {

    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }

    @Test
    public void naoPodeAlistarElfo() {
        Elfo elfo = new Elfo("Common Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfo = new ElfoDaLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void buscarElfosVivosExistindo() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(vivo)
            );
        assertEquals(esperado, exercito.buscar(Status.VIVO));
    }

    @Test
    public void buscarElfosVivosNaoExistindo() {
        Elfo vivoTrocaPraMorto = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        vivoTrocaPraMorto.setStatus(Status.MORTO);
        exercito.alistar(vivoTrocaPraMorto);
        assertNull(exercito.buscar(Status.VIVO));
    }

    @Test
    public void buscarElfosMortosExistindo() {
        Elfo morto = new ElfoVerde("Undead Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        morto.setStatus(Status.MORTO);
        exercito.alistar(morto);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(morto)
            );
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }    

    @Test
    public void buscarElfosMortosNaoExistindo() {
        Elfo vivo = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(vivo);
        assertNull(exercito.buscar(Status.MORTO));
    }
}


import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test
{   
    @Test
    public void testeDado()
    {
        DadoD6 dado = new DadoD6(); 
        ArrayList<Integer> numerosPermitidos = new ArrayList<>();
        numerosPermitidos.add(1);
        numerosPermitidos.add(2);
        numerosPermitidos.add(3);
        numerosPermitidos.add(4);
        numerosPermitidos.add(5);
        numerosPermitidos.add(6);
        assertTrue(numerosPermitidos.contains(dado.sortear()));
    }
}

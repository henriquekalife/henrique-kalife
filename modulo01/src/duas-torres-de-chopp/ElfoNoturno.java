public class ElfoNoturno extends Elfo {
    private static int contadorDeInstancias = 0;
    
    public ElfoNoturno(String nome) {
        super(nome);
        QTD_DANO = 15.;
        QTD_EXPERIENCIA *= 3;
        contadorDeInstancias++;
    }
    
    public static int getContadorDeInstancias () {
        return contadorDeInstancias;
    }
    
    public static void zeraContador () {
        contadorDeInstancias = 0;
    }
}
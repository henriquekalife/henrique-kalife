import java.util.*;

public class ElfoDaLuz extends Elfo {
    private int qtdAtaques = 0;
    private static final double QTD_VIDA_GANHA = 10;

    private static final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
                "Espada de Galvorn"
            ));

    public ElfoDaLuz(String nome) {
        super(nome);
        QTD_DANO = 21;
        super.ganharItem(new ItemImperdivel(DESCRICOES_OBRIGATORIAS.get(0), 1));
    }

    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }

    private void ganharVida() {
        vida += ElfoDaLuz.QTD_VIDA_GANHA;
    }

    @Override
    public void perderItem(Item item) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder) {
            super.perderItem(item);
        }
    }

    private Item getEspada() {
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }

    public void atacarComEspada(Dwarf dwarf) {
        Item espada = getEspada();
        if (espada.getQuantidade() > 0) {
            qtdAtaques++;
            dwarf.perderVida();
            if (devePerderVida()) {
                perderVida();
            } else {
                ganharVida();
            }
        }
    }
}
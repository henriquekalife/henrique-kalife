import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void consultarNome () {
        AgendaContatos agenda = new AgendaContatos();
	agenda.adicionar("Bernardo", "555555");
	assertEquals("555555", agenda.consultarPeloNome("Bernardo"));
    }
    
    
    /*
    @Test
    public void consultarTelefone () {
        AgendaContatos agenda = new AgendaContatos();
	agenda.adicionar("Bernardo", "555555");
	agenda.adicionar("Mithrandir", "444444");
	assertEquals("Mithrandir", agenda.consultarPeloTelefone("444444"));
    }
    
    @Test
    public void testeCSV () {
        AgendaContatos agenda = new AgendaContatos();
	agenda.adicionar("Bernardo", "555555");
	agenda.adicionar("Mithrandir", "444444");
	assertEquals("Bernardo,555555", agenda.csv("Bernardo", "555555"));
    }
    */
    
}

public class FormaGeometricaFactory
{
    public static Retangulo criar (int tipo, int x, int y) {
        Retangulo formaGeometrica = null;
        switch(tipo) {
            case 1:
                //criar retangulo
                formaGeometrica = new Retangulo();
                break;
            case 2:
                //criar quadrado
                formaGeometrica = new Quadrado();
                break;
        }
        formaGeometrica.setX(x);
        formaGeometrica.setY(y);
        return formaGeometrica;
    }
}

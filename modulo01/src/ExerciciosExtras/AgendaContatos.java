import java.util.*;

public class AgendaContatos
{
    private HashMap <String, String> agenda = new HashMap<>();
    
    public AgendaContatos () {}
    
    //adicionar um contato (nome e telefone)
    public void adicionar (String nome, String telefone) {
        agenda.put(nome, telefone);
    }

    //obter o telefone a partir de um nome
    public String consultarPeloNome (String nome) {
        return agenda.get(nome);
    }
    
    //pesquisar um contato a partir do telefone (sem criar um outro hashmap)
    public String consultarPeloTelefone (String telefone) {
        return agenda.get(telefone);
    }
    
    //retornar uma String em formato CSV
    public String csv (String nome, String telefone) {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for (HashMap.Entry<String, String> par: agenda.entrySet()) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s, %s ", chave, valor, separador);
            builder.append(contato);
        }
        
        return builder.toString();
    }
}
